import { LoginPage } from "../../pages/loginpage";

const loginpage = new LoginPage();

describe("Login Flow", () => {
  beforeEach(() => {
    cy.visit("https://platform.wowzi.co/auth/login");
    cy.findByText("Log in to your Wowzi advertiser account").should(
      "be.visible"
    );
  });
  it("Doesnt allow users if the email field is empty but the password field has a valid value", () => {
    loginpage.loginWithJustPassword();
  });

  it("Doesnt allow users if the password field is empty but the email field has a valid value", () => {
    loginpage.loginWithJustEmail();
  });

  it("Doesnt allow users if both the email and password fields are empty", () => {
    loginpage.loginWithEmptyValues();
  });

  it("Doesnt allow users if the email field has an invalid email but a valid password", () => {
    loginpage.loginWithInvalidEmail();
  });

  it("Doesnt allow users if the email field is correct and valid value but the password is incorrect", () => {
    loginpage.loginWithIncorrectPassword();
  });

  it("Doesnt allow users if the email is valid but incorrect but the password is correct", () => {
    loginpage.loginWithIncorrectEmail();
  });
  it("Allows users if the email and password combination are valid and correct", () => {
    loginpage.loginWithValidCredentials();
  });
});
