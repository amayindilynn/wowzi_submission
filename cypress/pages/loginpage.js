
const emailAddress = "amayindilynn@gmail.com";
const passwordCombination = "P@$sw0rd";


export class LoginPage {  

  errorMessage = () => cy.get(`[class*="snackbar__message"]`);

  loginWithJustPassword() {
    cy.get("input[name=password]").type(passwordCombination);
    cy.get("button").click();
    this.errorMessage().contains("Invalid input data").should("be.visible");
  }

  loginWithJustEmail() {
    cy.get("input[name=email]").type(emailAddress);
    cy.get("button").click();
    this.errorMessage().contains("Invalid input data").should("be.visible");
  }

  loginWithEmptyValues() {
    cy.get("button").click();
    this.errorMessage().contains("Invalid input data").should("be.visible");
  }

  loginWithInvalidEmail() {
    cy.get("input[name=email]").type("amayindilynn");
    cy.get("input[name=password]").type(passwordCombination);
    cy.get("button").click();
    cy.findByText("Log in to your Wowzi advertiser account").should(
      "be.visible"
    );
  }

  loginWithIncorrectPassword() {
    cy.get("input[name=email]").type(emailAddress);
    cy.get("input[name=password]").type("P@$sw");
    cy.get("button").click();
    this.errorMessage().contains("Invalid username or password.").should("be.visible");
  }

  loginWithIncorrectEmail() {
    cy.get("input[name=email]").type("amayindily@gmail.com");
    cy.get("input[name=password]").type(passwordCombination);
    cy.get("button").click();
    cy.contains("E-mail is not verified yet.").should("be.visible");
  }

  loginWithValidCredentials() {
    cy.get("input[name=email]").type(emailAddress);
    cy.get("input[name=password]").type(passwordCombination);
    cy.get("button").click();
    cy.intercept("GET", "https://api.wowzi.co/campaigns/dashboard").as(
      "getValidation"
    );
    cy.contains("No upcoming campaigns").should("be.visible");
    cy.get('h6').contains(" Managing your influencer campaigns is easier than ever with Wowzi. Let us show you around! ").should("be.visible");
    cy.wait("@getValidation").its("response.statusCode").should("eq", 200);
  }
}
