# wowzi_submission

cd existing_repo
git remote add origin https://gitlab.com/amayindilynn/wowzi_submission.git
git branch -M main
git push -uf origin main


## Integrate with your tools
This test has been integrated with cypress dashboard

## Test and Deploy
The test has also been setup on gitlab and it runs on every merge request
The test report is sent to cypress dashboard


***
## Description
This test is for the login feature of the Wowzi application

## Installation
To run the test, after cloning:
run npm install
npx cypress run to run in headless mode
npx cypress open (initializes a browser)
npx cypress run --browser chrome (for running on chrome)
npx cypress run --browser firefox (for running on firefox)

